package com.myzee.rest.model;

public class DataMap {
	private int dataId;
	private String dataName;
	private String dataDesc;
	
	public DataMap(int dataId, String dataName, String dataDesc) {
		this.dataId = dataId;
		this.dataName = dataName;
		this.dataDesc = dataDesc;
	}

	public int getDataId() {
		return dataId;
	}

	public void setDataId(int dataId) {
		this.dataId = dataId;
	}

	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getDataDesc() {
		return dataDesc;
	}

	public void setDataDesc(String dataDesc) {
		this.dataDesc = dataDesc;
	}
	
}
