package com.myzee.rest.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.myzee.rest.model.DataMap;

@Repository
public class DataDAO {
	public static DataMapList list = new DataMapList();
	
	static 
	{
		DataMap d1 = new DataMap(1, "alice", "Gen.author");
		DataMap d2 = new DataMap(2, "bob", "author");
		DataMap d3 = new DataMap(3, "ketherine", "Ja.author");
		List<DataMap> l = list.getList();
//		l.add(d1);
//		l.add(d2);
//		l.add(d3);
//		list.setList(l);
		list.getList().add(d1);
		list.getList().add(d2);
		list.getList().add(d3);
		list.setList(list.getList());
	}
	
	public DataMapList getDataList() {
		return list;
	}
	
	public void setDataList(DataMap d) {
		list.getList().add(d);
	}
}
