package com.myzee.rest.dao;

import java.util.ArrayList;
import java.util.List;

import com.myzee.rest.model.DataMap;

public class DataMapList {
	private List<DataMap> list ;
	
	public List<DataMap> getList() {
		if(list == null) {
			return new ArrayList<>();
		}
		return list;
	}
	public void setList(List<DataMap> list) {
		this.list = list;
	}
}
