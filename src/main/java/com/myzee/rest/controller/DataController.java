package com.myzee.rest.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.myzee.rest.dao.DataDAO;
import com.myzee.rest.dao.DataMapList;
import com.myzee.rest.model.DataMap;

@RestController
@RequestMapping(path = "/datacheck")
public class DataController {
	
	@Autowired
	DataDAO dataDao;
	
	@GetMapping(path = "/getdata", produces = "application/json")
	public DataMapList getData() {
		return dataDao.getDataList();
	}
	
	@PostMapping(path = "/putdata", consumes = "application/json", produces="application/json")
	public ResponseEntity<Object> setData(@RequestBody DataMap d) {
		int incrId = dataDao.getDataList().getList().size() + 1;
		d.setDataId(incrId);
		dataDao.setDataList(d);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(d.getDataId())
                .toUri();

		return ResponseEntity.created(location).build();
	}
}
